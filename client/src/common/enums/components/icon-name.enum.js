const IconName = {
  USER_CIRCLE: 'user circle',
  LOG_OUT: 'log out',
  EDIT: 'pencil',
  THUMBS_UP: 'thumbs up',
  THUMBS_DOWN: 'thumbs down',
  COMMENT: 'comment',
  SHARE_ALTERNATE: 'share alternate',
  FROWN: 'frown',
  IMAGE: 'image',
  COPY: 'copy'
};

export { IconName };
