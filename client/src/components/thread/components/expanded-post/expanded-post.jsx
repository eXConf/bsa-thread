import * as React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { threadActionCreator } from 'src/store/actions';
import { Spinner, Post, Modal, Comment as CommentUI } from 'src/components/common/common';
import UpdatePost from '../update-post/update-post';
import AddComment from '../add-comment/add-comment';
import Comment from '../comment/comment';
import { getSortedComments } from './helpers/helpers';

const ExpandedPost = ({
  editPostId,
  setEditPostId,
  handlePostEdit,
  uploadImage,
  sharePost
}) => {
  const dispatch = useDispatch();
  const { post, userId } = useSelector(state => ({
    post: state.posts.expandedPost,
    userId: state.profile.user.id
  }));

  const handlePostLike = React.useCallback(id => (
    dispatch(threadActionCreator.likePost(id))
  ), [dispatch]);

  const handlePostDislike = React.useCallback(id => (
    dispatch(threadActionCreator.dislikePost(id))
  ), [dispatch]);

  const handleCommentAdd = React.useCallback(commentPayload => (
    dispatch(threadActionCreator.addComment(commentPayload))
  ), [dispatch]);

  const handleExpandedPostToggle = React.useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const handleExpandedPostClose = () => handleExpandedPostToggle();

  const sortedComments = getSortedComments(post.comments ?? []);

  return (
    <Modal
      dimmer="blurring"
      centered={false}
      open
      onClose={handleExpandedPostClose}
    >
      {post ? (
        <Modal.Content>
          {post.id === editPostId ? (
            <UpdatePost
              post={post}
              clearEditPostId={() => setEditPostId(undefined)}
              onPostUpdate={handlePostEdit}
              uploadImage={uploadImage}
              key={post.id}
            />
          ) : (
            <Post
              post={post}
              onEditClicked={() => setEditPostId(post.id)}
              isOwnedByUser={userId === post.user.id}
              onPostLike={handlePostLike}
              onPostDislike={handlePostDislike}
              onExpandedPostToggle={handleExpandedPostToggle}
              sharePost={sharePost}
            />
          )}
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <h3>Comments</h3>
            {sortedComments.map(comment => (
              <Comment key={comment.id} comment={comment} />
            ))}
            <AddComment postId={post.id} onCommentAdd={handleCommentAdd} />
          </CommentUI.Group>
        </Modal.Content>
      ) : (
        <Spinner />
      )}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  editPostId: PropTypes.string.isRequired,
  setEditPostId: PropTypes.func.isRequired,
  handlePostEdit: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired
};

export default ExpandedPost;
