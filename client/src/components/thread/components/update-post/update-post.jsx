import * as React from 'react';
import PropTypes from 'prop-types';
import { postType } from 'src/common/prop-types/prop-types';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';
import { Button, Form, Image, Segment } from 'src/components/common/common';

import styles from './styles.module.scss';

const UpdatePost = ({ post, clearEditPostId, onPostUpdate, uploadImage }) => {
  const [body, setBody] = React.useState(post.body);
  const [image, setImage] = React.useState(post.image);
  const [isUploading, setIsUploading] = React.useState(false);

  React.useEffect(() => {
    setImage(post.image);
  }, [post.image]);

  const handleUpdatePost = async () => {
    if (!body) {
      return;
    }
    await onPostUpdate({ id: post.id, userId: post.userId, imageId: image?.id, body });
    setBody('');
    setImage(undefined);
    clearEditPostId();
  };

  const handleUploadFile = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    uploadImage(file)
      .then(({ id, link }) => {
        setImage({ id, link });
      })
      .catch(() => {
        // TODO: show error
      })
      .finally(() => {
        setIsUploading(false);
      });
  };

  return (
    <Segment>
      <Form onSubmit={handleUpdatePost}>
        <Form.TextArea
          name="body"
          value={body}
          placeholder="Please write at least something"
          onChange={ev => setBody(ev.target.value)}
        />
        {image?.link && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image?.link} alt="post" />
          </div>
        )}
        <div className={styles.btnWrapper}>
          {!image && (
            <Button
              color="teal"
              isLoading={isUploading}
              isDisabled={isUploading}
              iconName={IconName.IMAGE}
            >
              <label className={styles.btnImgLabel}>
                Attach image
                <input
                  name="image"
                  type="file"
                  onChange={handleUploadFile}
                  hidden
                />
              </label>
            </Button>
          )}
          <Button onClick={clearEditPostId} color={ButtonColor.BLUE} type={ButtonType.BUTTON}>
            Cancel
          </Button>
          <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
            Update
          </Button>
        </div>
      </Form>
    </Segment>
  );
};

UpdatePost.propTypes = {
  post: postType.isRequired,
  clearEditPostId: PropTypes.func.isRequired,
  onPostUpdate: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

export default UpdatePost;
