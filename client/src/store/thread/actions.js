import { createAction } from '@reduxjs/toolkit';
import {
  comment as commentService,
  post as postService
} from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  UPDATE_POST: 'thread/update-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post'
};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));

const updatePost = createAction(ActionType.UPDATE_POST, post => ({
  payload: {
    post
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const editPost = post => async (dispatch, getRootState) => {
  const { id } = await postService.editPost(post);
  const editedPost = await postService.getPost(id);
  dispatch(updatePost(editedPost));

  const {
    posts: { expandedPost }
  } = getRootState();

  if (expandedPost && expandedPost.id === editedPost.id) {
    dispatch(setExpandedPost(editedPost));
  }
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPost(post));
};

const updateReactionCounts = ({ likeCount, dislikeCount, postId, dispatch, getRootState }) => {
  const mapReactionCounts = post => ({
    ...post,
    likeCount,
    dislikeCount
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapReactionCounts(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapReactionCounts(expandedPost)));
  }
};

const likePost = postId => async (dispatch, getRootState) => {
  const { likeCount, dislikeCount } = await postService.likePost(postId);
  updateReactionCounts({ likeCount, dislikeCount, postId, dispatch, getRootState });
};

const dislikePost = postId => async (dispatch, getRootState) => {
  const { likeCount, dislikeCount } = await postService.dislikePost(postId);
  updateReactionCounts({ likeCount, dislikeCount, postId, dispatch, getRootState });
};

const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

export {
  setPosts,
  addMorePosts,
  addPost,
  updatePost,
  setExpandedPost,
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  editPost,
  toggleExpandedPost,
  likePost,
  dislikePost,
  addComment
};
